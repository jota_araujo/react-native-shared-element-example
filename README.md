# React Native Shared Element Example

![screenshot](https://bitbucket.org/jota_araujo/react-native-shared-element-example/raw/80c9f971d4d352b553cde2de6e59d7bb101361d7/shared-screen-elment.gif)


### Technologies/solutions featured in this project

- React Native / Expo SDK
- Typescript+Eslint via [@typescript-eslint/parser](https://www.npmjs.com/package/@typescript-eslint/parser)
- React Navigation 4.x
- React Native Gesture Handlers
- SVG file imports


![screenshot](https://bitbucket.org/jota_araujo/react-native-shared-element-example/raw/8a6ffc561e58e350462a9f285ce5455e04b09ba5/screenshot1.jpg) ![screenshot](https://bitbucket.org/jota_araujo/react-native-shared-element-example/raw/8a6ffc561e58e350462a9f285ce5455e04b09ba5/screenshot2.jpg)


Based on a original concept design by [Julia @ Dribbble](https://dribbble.com/lorelai891)
