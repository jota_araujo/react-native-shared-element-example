import React from "react";
import { StatusBar } from "react-native";
import { createAppContainer } from "react-navigation";
import { createSharedElementStackNavigator } from "react-navigation-shared-element";

import LoadAssets from "./src/components/LoadAssets";
import CoverScreen from "./src/CoverScreen";
import DetailScreen from "./src/DetailScreen";

const fonts = {
  "SFProText-Bold": require("./assets/fonts/SF-Pro-Text-Bold.otf"),
  "SFProText-Semibold": require("./assets/fonts/SF-Pro-Text-Semibold.otf"),
  "SFProText-Regular": require("./assets/fonts/SF-Pro-Text-Regular.otf"),
  "Typewriter-Regular": require("./assets/fonts/TYPEWR.ttf"),
  "Typewriter-Bold": require("./assets/fonts/TYPEWR-Bold.ttf")
};

const stackNavigator = createSharedElementStackNavigator(
  {
    CoverScreen,
    DetailScreen
  },
  {
    initialRouteName: "CoverScreen",
    mode: "modal",
    headerMode: "none",
    defaultNavigationOptions: {
      gestureEnabled: true,
      gestureDirection: "horizontal"
    }
  }
);

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
const AppNavigator = createAppContainer(stackNavigator);

export default () => (
  <LoadAssets {...{ fonts }}>
    <StatusBar barStyle="light-content" />
    <AppNavigator />
  </LoadAssets>
);
