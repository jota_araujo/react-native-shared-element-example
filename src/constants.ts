/* eslint-disable max-len */
import { Dimensions } from "react-native";

export const Colors = {
  RED: "#f2d802",
  YELLOW: "#f2d802"
};

export type Brand = {
  name: string;
  src: number;
};

const { width } = Dimensions.get("window");
export const SCREEN_WIDTH = width;

export const article = `
Less is More – This is the first rule of the nude makeup look. You will have to unlearn whatever you’ve learned about applying foundations and concealers. You should not look caked-up while trying to look natural. Use foundation carefully and know when to stop!

Try to Look as Natural as Possible – This should be your main focus. Choose the shades that look natural on you. If you have pale skin, opt for pink or peachy colours and if you have olive undertones, opt for pale beige or cream colours. Lip colour should also look natural. Instead of matte lipstick, apply a little gloss on your lips directly.

Invest in Grooming Sessions – Since you are not allowed to use a lot of makeup in order to hide your flaws, you have got to invest a little on grooming. The best time to carry this look is after getting a facial or after giving your skin a proper cleansing/toning ritual. You need to take care of your lips as well, exfoliate them properly.

The Magic Mascara – You don’t really need oodles of eye shadows and liners when you can look fabulous by putting a little mascara alone. We all know how wonderfully mascara works on our eyes. Just trust your old buddy and give it a chance. If you want, you can add false lashes or a stroke of an eye pencil to give a glamorous twist to your final look.

Blush – You can use blush, but make sure it compliments your skin tone. Do not opt for shades that look contrasting and also, keep a check on the amount of blush being used. Powder based blushes are usually preferred for the nude look.

Lip – There are so many new nude lipsticks, lip glosses now available on the market. Pick the one from your favourite brand and apply it precisely with a lip brush.
`;
