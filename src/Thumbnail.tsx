import React from "react";
import { StyleSheet, View } from "react-native";
import Animated from "react-native-reanimated";
import Text from "./components/Text";
import { Brand, Colors } from "./constants";

export const THUMB_SIZE = 120;

const styles = StyleSheet.create({
  container: {
    width: THUMB_SIZE,
    height: THUMB_SIZE
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    width: undefined,
    height: undefined,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10
  },
  badge: {
    position: "absolute",
    borderBottomLeftRadius: 12,
    borderBottomRightRadius: 12,
    borderTopRightRadius: 12,
    borderTopLeftRadius: 12,
    backgroundColor: Colors.YELLOW,
    top: 10,
    left: 10,
    padding: 4
  }
});

type ThumbnailProps = {
  brand: Brand;
  showBadge: boolean;
};

export default ({ brand, showBadge }: ThumbnailProps) => {
  return (
    <View style={styles.container}>
      <Animated.Image style={[styles.image]} source={brand.src} />
      {showBadge && (
        <View style={styles.badge}>
          <Text type="verySmallBold">{brand.name}</Text>
        </View>
      )}
    </View>
  );
};
