import React from "react";
import { ScrollView, StyleSheet } from "react-native";
import Animated from "react-native-reanimated";
import { SharedElement } from "react-navigation-shared-element";
import { useNavigation } from "react-navigation-hooks";
import { TransitionSpecs } from "react-navigation-stack";
import { Brand, article } from "./constants";
import Logo from "./components/Logo";
import Text from "./components/Text";

export const THUMB_SIZE = 120;

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "white"
  },
  image: {
    width: undefined,
    height: 400,
    borderBottomRightRadius: 60
  },
  title: {
    marginTop: 24,
    marginLeft: 24
  },
  article: {
    marginTop: 16,
    marginLeft: 24,
    marginRight: 24,
    marginBottom: 24,
    textAlign: "justify"
  }
});

const DetailScreen = () => {
  const { getParam } = useNavigation();
  const brand: Brand = getParam("brand");

  return (
    <ScrollView style={styles.container}>
      <SharedElement id={brand.name}>
        <Animated.Image style={[styles.image]} source={brand.src} />
      </SharedElement>
      <Text type="title2" style={styles.title}>
        6 Easy Tips for Nude Makeup Look
      </Text>
      <Text type="body" style={styles.article}>
        {article}
      </Text>
      <Logo />
    </ScrollView>
  );
};

DetailScreen.sharedElements = (
  navigation: ReturnType<typeof useNavigation>
) => {
  const brand = navigation.getParam("brand");
  return [brand.name];
};
DetailScreen.navigationOptions = {
  transitionSpec: {
    open: TransitionSpecs.TransitionIOSSpec,
    close: TransitionSpecs.TransitionIOSSpec
  },
  cardStyleInterpolator: ({ current, layouts }) => ({
    cardStyle: {
      transform: [
        {
          translateX: current.progress.interpolate({
            inputRange: [0, 1],
            outputRange: [layouts.screen.width, 0]
          })
        }
      ],
      overlayStyle: {
        opacity: current.progress.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 1]
        })
      }
    }
  })
};

export default DetailScreen;
