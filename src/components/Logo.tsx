import React from "react";
import { View } from "react-native";
import VogueLogo from "../assets/vogue_logo.svg";

export default () => {
  return (
    <View
      style={{
        position: "absolute",
        top: 50,
        left: 0,
        paddingTop: 6,
        paddingBottom: 6,
        paddingLeft: 80,
        width: 250,
        height: 55,
        backgroundColor: "yellow"
      }}
    >
      <VogueLogo width={150} height={40} />
    </View>
  );
};
