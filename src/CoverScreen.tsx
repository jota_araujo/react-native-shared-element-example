import React from "react";
import { useIsFocused, useNavigation } from "react-navigation-hooks";
import { SharedElement } from "react-navigation-shared-element";
import { ImageBackground, StyleSheet, View } from "react-native";
import {
  PanGestureHandler,
  State,
  TouchableWithoutFeedback
} from "react-native-gesture-handler";
import Animated, { Value, add, interpolate } from "react-native-reanimated";
import { clamp, onGestureEvent, withOffset } from "react-native-redash";
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import { BoxShadow } from "react-native-shadow";
import Text from "./components/Text";
import Logo from "./components/Logo";
import Thumbnail, { THUMB_SIZE } from "./Thumbnail";
import { Brand, Colors, SCREEN_WIDTH } from "./constants";

const GALLERY_WIDTH = SCREEN_WIDTH;
const RULER_HEIGHT = THUMB_SIZE;

const DATA = [
  {
    name: "ysl",
    src: require("./assets/thumb_2.jpg")
  },
  {
    name: "givenchy",
    src: require("./assets/thumb_0.jpg")
  },
  {
    name: "versace",
    src: require("./assets/thumb_4.jpg")
  },
  {
    name: "dior",
    src: require("./assets/thumb_5.jpg")
  }
];

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    paddingBottom: 50
  },
  gallery: {
    width: GALLERY_WIDTH,
    height: THUMB_SIZE,
    justifyContent: "center"
  },
  galleryBackground: {
    height: RULER_HEIGHT
  },
  titleContainer: {
    marginLeft: 24,
    marginBottom: 80,
    width: SCREEN_WIDTH,
    elevation: 24
  },
  topicContainer: {
    flexDirection: "row",
    marginBottom: 16
  },
  topicText: {
    color: "white",
    marginRight: 6
  }
});

const shadowOpt = {
  width: THUMB_SIZE,
  height: THUMB_SIZE,
  color: "#000",
  radius: 10,
  opacity: 0.2,
  x: 4,
  y: 6
};

type GalleryItemProps = {
  data: Brand;
  x: Animated.Node<number>;
  offset: number;
};

const GalleryItem = ({ data, offset, x }: GalleryItemProps) => {
  const { navigate } = useNavigation();
  const isFocused = useIsFocused();

  const translateX = add(x, offset);
  const scale = interpolate(translateX, {
    inputRange: [-THUMB_SIZE, (GALLERY_WIDTH - THUMB_SIZE) / 2, GALLERY_WIDTH],
    outputRange: [0.6, 1, 0.6]
  });

  return (
    <Animated.View
      style={{
        position: "absolute",
        top: 0,
        left: 0,
        transform: [{ translateX, scale }]
      }}
    >
      <BoxShadow setting={shadowOpt}>
        <Animated.View
          style={{
            ...StyleSheet.absoluteFillObject,
            transform: [{}]
          }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              navigate("DetailScreen", { brand: data });
            }}
          >
            <SharedElement id={data.name}>
              <Thumbnail brand={data} showBadge={isFocused} />
            </SharedElement>
          </TouchableWithoutFeedback>
        </Animated.View>
      </BoxShadow>
    </Animated.View>
  );
};

const Topic = () => {
  return (
    <View style={styles.topicContainer}>
      <Text type="smallBold" style={styles.topicText}>
        makeup
      </Text>
      <Text type="smallBold" style={[styles.topicText, { color: Colors.RED }]}>
        /
      </Text>
      <Text type="smallBold" style={styles.topicText}>
        dior
      </Text>
    </View>
  );
};

const CoverScreen = () => {
  const state = new Value(State.UNDETERMINED);
  const translationX = new Value(0);
  const gestureHandler = onGestureEvent({ state, translationX });
  const x = clamp(
    withOffset(translationX, state),
    -SCREEN_WIDTH / 2,
    (SCREEN_WIDTH - THUMB_SIZE) / 2
  );

  return (
    <ImageBackground
      source={require("./assets/cover_photo.jpg")}
      style={{ width: "100%", height: "100%" }}
    >
      <View style={styles.container}>
        <Logo />
        <View style={styles.titleContainer}>
          <Topic />
          <Text type="title1" style={{ color: "white" }}>
            {"dior haute\ncouture spring\n/ summer 2020"}
          </Text>
        </View>
        <PanGestureHandler
          activeOffsetX={10}
          activeOffsetY={10}
          minDist={5}
          {...gestureHandler}
        >
          <Animated.View style={styles.gallery}>
            <View>
              <View style={styles.galleryBackground} />
            </View>
            {DATA.map((el, index) => {
              return (
                <GalleryItem
                  key={index}
                  data={el}
                  x={x}
                  offset={THUMB_SIZE * index}
                />
              );
            })}
          </Animated.View>
        </PanGestureHandler>
      </View>
    </ImageBackground>
  );
};

export default CoverScreen;
